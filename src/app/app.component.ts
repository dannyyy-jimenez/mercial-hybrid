import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NoAuthComponent } from '../pages/NoAuth/noauth.component';
import {Keyboard} from '@ionic-native/keyboard';
import { AuthService } from './auth/auth.service';
import { AuthComponent } from '../pages/Auth/auth.component';
import { Subscription } from 'rxjs';
import { Network } from '@ionic-native/network';
import { SnackbarService } from './snackbar/snackbar.service';
import { TranslateService } from '@ngx-translate/core';
import { timer } from 'rxjs/observable/timer';
import { Globalization } from '@ionic-native/globalization';

declare var nativeclick: { trigger: () => void };

@Component({
  templateUrl: 'app.html'
})
export class Mercial {
  rootPage:any;
  pages : Array<{ title: string, component: any }>;
  disconnectSub : Subscription;
  connectSub : Subscription;
  online : boolean;

  constructor(platform: Platform, private authService : AuthService, private keyboard: Keyboard, statusBar: StatusBar, splashScreen: SplashScreen, private snackbarService : SnackbarService, private network: Network, private translator : TranslateService, private globalization: Globalization) {
    this.translator.setDefaultLang("en");
    this.translator.use("en.client");
    this.authService.isAuthenticated().then(isAuthenticated => {
      if (isAuthenticated) {
        this.rootPage = AuthComponent;
      } else {
        this.rootPage = NoAuthComponent;
      }
    });
    platform.ready().then((val) => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.overlaysWebView(true);
      statusBar.backgroundColorByHexString('#e7e7e7');

      if (platform.is('ios')) {
        this.keyboard.disableScroll(true);
      }
      timer(3000).subscribe(() => {
        splashScreen.hide();
      });
      this.disconnectSub = this.network.onDisconnect().subscribe(() => {
        this.snackbarService.handleError({ message: "network_disconnected", isError: true });
      });
      this.connectSub = this.network.onConnect().subscribe(() => {
        this.snackbarService.handleError({ message: "network_connected", isError: false });
      });
      const nativeClickListener = (event: Event) => {
            // Traverse through the clicked element and all ancestors.
            for (
                let curElement = <Element> event.target;
                curElement != null;
                curElement = curElement.parentElement
            ) {
                // If a BUTTON element is encountered, trigger a click and stop.
                if (curElement.tagName === 'BUTTON') {
                  // ‘nativeclick’ doesn't exist outside Cordova's environment.
                  typeof nativeclick !== 'undefined' && nativeclick.trigger();
                  break;
                }
            }
        };
        // Call the above listener whenever anything is clicked, ensuring that it
        // is called before more specific EventTargets (or else clicks won't be
        // heard on e.g. <ion-datetime> components or <ion-navbar> back buttons).
        document.addEventListener('click', nativeClickListener, true);
    });
  }
}
