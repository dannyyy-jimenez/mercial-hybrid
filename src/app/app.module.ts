import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { Mercial } from './app.component';
import { NoAuthComponent } from '../pages/NoAuth/noauth.component';
import { LandingComponent } from '../pages/NoAuth/landing/landing.component';
import { LoginComponent } from '../pages/NoAuth/login/login.component';
import { RegisterComponent } from '../pages/NoAuth/register/register.component';
import { ForgotPasswordComponent } from '../pages/NoAuth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../pages/NoAuth/reset-password/reset-password.component';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import { AuthService } from './auth/auth.service';
import { APIService } from './auth/api.service';
import { SnackbarService } from './snackbar/snackbar.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LoaderModule } from './loader/loader.module';
import { LoaderService } from './loader/loader.service';
import { AnalyticsComponent } from '../pages/Auth/analytics/analytics.component';
import { RedeemValidatorComponent } from '../pages/Auth/validator/redeem-validator.component';
import { SettingsComponent } from '../pages/Auth/settings/settings.component';
import { OffersComponent } from '../pages/Auth/offers/offers.component';
import { HelpComponent } from '../pages/Auth/help/help.component';
import { FeedComponent } from '../pages/Auth/feed/feed.component';
import { RedeemComponent } from '../pages/Auth/offers/redeem/redeem.component';
import { OfferComponent } from '../pages/Auth/offers/offer/offer.component';
import { ViewerComponent } from '../pages/Auth/feed/viewer/viewer.component';
import { MercialComponent } from '../pages/Auth/feed/mercial/mercial.component';
import { AuthComponent } from '../pages/Auth/auth.component';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';

import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { IonicCacheSrcModule } from 'ionic-cache-src';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { Network } from '@ionic-native/network';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { MessengerComponent } from '../pages/Auth/help/messenger/messenger.component';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HelpViewerComponent } from '../pages/Auth/help/viewer/viewer.helper.component';
import { HelperViewerService } from '../pages/Auth/help/viewer/viewer.helper.service';

import { MercialViewerService } from '../pages/Auth/feed/viewer/viewer.mercial.service';
import { OfferViewerService } from '../pages/Auth/offers/redeem/viewer.offer.service';
import { Globalization } from '@ionic-native/globalization';
import { LegalViewerComponent } from '../pages/Legal/legal.viewer.component';

const config: SocketIoConfig = { url: process.env.BASE_URL ? process.env.BASE_URL : 'https://www.mercial.io', options: {path: '/io/listener/', secure: true} };
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, `https://res.cloudinary.com/aurodim/raw/upload/v${process.env.VERSION}/mercial/static/translations/`, ".json");
}
const GoogleMapsCore = AgmCoreModule.forRoot({
  apiKey : 'AIzaSyCN7Lr-FZ2irzlktTPr23ewRK1fSHcWQ9s',
  libraries: ['places']
});

@NgModule({
  declarations: [
    Mercial,
    NoAuthComponent,
    AuthComponent,
    LegalViewerComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    AnalyticsComponent,
    FeedComponent,
    MercialComponent,
    ViewerComponent,
    HelpViewerComponent,
    HelpComponent,
    MessengerComponent,
    OffersComponent,
    OfferComponent,
    RedeemComponent,
    SettingsComponent,
    RedeemValidatorComponent
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
       loader: {
           provide: TranslateLoader,
           useFactory: HttpLoaderFactory,
           deps: [HttpClient]
       }
    }),
    IonicModule.forRoot(Mercial,
      {
        autocomplete: 'on',
        platforms : {
          ios : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
          },
          android : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
          }
          // http://ionicframework.com/docs/v2/api/config/Config/)
        }
      }
    ),
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'aurodim' } as CloudinaryConfiguration),
    IonicStorageModule.forRoot(),
    IonicCacheSrcModule,
    SocketIoModule.forRoot(config),
    InfiniteScrollModule,
    HttpClientModule,
    LoaderModule,
    ChartsModule,
    GoogleMapsCore
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Mercial,
    AuthComponent,
    NoAuthComponent,
    LegalViewerComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    AnalyticsComponent,
    FeedComponent,
    MercialComponent,
    ViewerComponent,
    HelpViewerComponent,
    HelpComponent,
    MessengerComponent,
    OffersComponent,
    OfferComponent,
    RedeemComponent,
    SettingsComponent,
    RedeemValidatorComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    DatePicker,
    AuthService,
    File,
    FileTransfer,
    SnackbarService,
    Globalization,
    LaunchNavigator,
    LoaderService,
    Network,
    APIService,
    HelperViewerService,
    MercialViewerService,
    OfferViewerService,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
