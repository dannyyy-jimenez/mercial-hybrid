import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/retry';
import { SnackbarService } from "../snackbar/snackbar.service";
import { LoaderService } from "../loader/loader.service";
import { App } from "ionic-angular";
import { NoAuthComponent } from "../../pages/NoAuth/noauth.component";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class APIService {
  public api : string;

  constructor(private http : HttpClient, private app : App, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService, private translate : TranslateService) {
    this.api = process.env.BASE_URL;
  }

  AUTH() {
    return this.authService.getAUTH();
  }

  // GET

  analyticsData(onError? : Function) {
    return this.http
    .get(this.api + "/api/user/analytics", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  feedData(previousPopulations = 0, filters : any, onError? : Function) {
    return this.http
    .get(this.api + "/api/user/feed", { params : { 'auth' : this.authService.getAUTH(), 'prevPop' : previousPopulations.toString(), 'date' : filters.date, 'meta' : filters.meta, 'categories' : filters.categories, 'lang' : this.translate.getDefaultLang()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  feedItem(id : string, onError? : Function) : Observable<Object> {
    return this.http
    .get(this.api + "/api/user/feed/"+id, { params : { 'auth' : this.authService.getAUTH(), 'lang' : this.translate.getDefaultLang()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  helpData(onError? : Function) {
    return this.http
    .get(this.api + "/api/user/help", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  offersData(previousPopulations = 0, filters : any, onError? : Function) {
    return this.http
    .get(this.api + "/api/user/offers", { params : { 'auth' : this.authService.getAUTH(), 'prevPop' : previousPopulations.toString(), 'date' : filters.date, 'meta' : filters.meta, 'location' : filters.location, 'lang' : this.translate.getDefaultLang()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  offerItem(id : string, onError? : Function) : Observable<Object> {
    return this.http
    .get(this.api + "/api/user/offers/"+id, { params : { 'auth' : this.authService.getAUTH(), 'lang' : this.translate.getDefaultLang()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  fastQR(values : any, onError? : Function) : Observable<Object> {
    return this.http
    .get(this.api + "/api/user/offers/"+values.id+"/fqr", { params : { 'auth' : this.authService.getAUTH(), 'offer' : values.id}, responseType : 'text' })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  settingsData(onError? : Function) {
    return this.http
    .get(this.api + "/api/user/settings", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  // POST
  public registerUser(requirements : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(requirements);
    return this.http.post(this.api + "/api/user/register", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  public loginUser(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/user/login", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  public forgot(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/user/forgot", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  public reset(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/user/reset", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  validateRedeem(values : any, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body : any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/offers/validate`, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  rateFranchise(values : any, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body : any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/rate`, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  enterZipcode(values : any, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body : any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/zes`, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  report(values : any, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body : any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/report`, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  addInterest(values : any, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body : any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/feed/${values.id}/interest`, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  mercialWatched(values : any, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body : any = JSON.stringify(values);
    return this.http.post(this.api + `/api/user/feed/${values.id}/watched`, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  updateAccount(values : object, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/user/settings/account", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  updateTarget(values : object, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/user/settings/target", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  updateLocation(values : object, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/user/settings/location", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  updatePreferences(values : object, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/user/settings/prefs", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  // updatePrivacy(values : object, onError? : Function) : Observable<Object> {
  //   const headers = new HttpHeaders({'Content-Type' : 'application/json'});
  //   const body = JSON.stringify(values);
  //   return this.http.post(this.api + "/api/user/settings/privacy", body, { headers : headers })
  //   .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  // }

  deleteAccount(values : object, onError? : Function) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/user/delete", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  ErrorHandler(error : HttpErrorResponse, onError? : Function) {
    this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
    if (error.status === 401) {
      this.authService.logoutUser();
      return Observable.empty<null>();
    }
    if (onError) onError();
    this.loaderService.handleLoader(false);
    return Observable.empty<null>();
  }
}
