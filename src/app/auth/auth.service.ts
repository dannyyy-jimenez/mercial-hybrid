import { Injectable, EventEmitter } from "@angular/core";
import * as jwt_decode from "jwt-decode";
import { Storage } from '@ionic/storage';
import { Socket } from 'ng-socket-io';

@Injectable()
export class AuthService {
  private loggedIn : boolean = false;
  private _at : string = null;
  private fullName : string = null;
  public merchips : number = null;
  private subscription : EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private storage: Storage, private socket : Socket) {
    this.storage.get("_fn").then(fn => {
      this.fullName = fn;
      return storage.get("_at")
    }).then(at => {
      this._at = at;
      return storage.get("_m")
    }).then(m => {
      this.merchips = m;
      if (this.fullName && this._at && this.merchips) {
        this.loggedIn = true;
      }
    });
  }

  // SETTERS
  public authenticateUser(body : any, cb : Function) {
    this.fullName = body._fn;
    this._at = body._tk;
    this.merchips = body._c;
    this.storage.set("_fn", this.fullName);
    this.storage.set("_at", this._at);
    this.storage.set("_m", this.merchips.toString());
    this.loggedIn = true;
    cb();
  }

  public setMerchips(merchips : number) {
    this.merchips = merchips;
    this.storage.set("_m", this.merchips.toString());
  }

  public logoutUser() {
    this.socket.disconnect();
    this.loggedIn = false;
    this.fullName = null;
    this._at = null;
    this.merchips = null;
    this.storage.remove("_fn");
    this.storage.remove("_at");
    this.storage.remove("_m");
    this.subscription.emit(true);
  }

  public getSubcription() {
    return this.subscription;
  }

  // GETTERS

  public getSocket() {
    const decodedToken = jwt_decode(this._at);
    return decodedToken._s;
  }

  public isAuthenticated() {
    return new Promise((resolve, reject) => {
      this.storage.get("_fn").then(fn => {
        this.fullName = fn;
        return this.storage.get("_at")
      }).then(at => {
        this._at = at;
        return this.storage.get("_m")
      }).then(m => {
        this.merchips = m;
        if (this.fullName && this._at && this.merchips) {
          this.loggedIn = true;
        }
        resolve(this.loggedIn);
      });
    });
  }

  public getMerchips() {
    let formatted = "";
    if (this.merchips / 1000000000 >= 1) {
        formatted += (this.merchips / 1000000000).toFixed(2) + "B";
    } else if (this.merchips / 1000000 >= 1) {
      formatted += (this.merchips / 1000000).toFixed(2) + "M";
    } else if (this.merchips / 1000 >= 1) {
      formatted += (this.merchips / 1000).toFixed(2) + "K";
    } else {
      return this.merchips;
    }

    let formattedArr = formatted.split(".");

    return formattedArr[1].substring(0,2) === "00" ? formattedArr[0] + formattedArr[1].substring(2) : formatted;
  }

  public getFullname() {
    return this.fullName;
  }

  public getAUTH() {
    return this._at;
  }
}
