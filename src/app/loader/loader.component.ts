import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { LoaderService } from "./loader.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'mercial-loader',
  templateUrl: './loader.component.html',
  styles: [`
  .backlay {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #f8f8f8;
    z-index: 1;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  `]
})
export class LoaderComponent implements OnInit, OnDestroy {
  @Input('awaiting') set awaiting(isInitiallyWanted: boolean) {
    if (isInitiallyWanted) {
      this.showLoader = true;
    } else {
      this.showLoader = false;
    }
    setTimeout(()=>{
      this.loaderService.loading = this.showLoader;
    }, 0);
  }
  showLoader : boolean;
  subscription : Subscription;

  constructor(private loaderService : LoaderService) {}

  ngOnInit() {
    // if (!this.awaiting) {
    //   setTimeout(()=>{
    //     this.loaderService.loading = this.showLoader;
    //   }, 0);
    // }
    this.subscription = this.loaderService.eventOcurred.subscribe(
      (show : boolean) => {
        this.showLoader = show;
      }
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
