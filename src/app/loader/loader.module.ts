import { CommonModule } from "@angular/common";
import { LoaderComponent } from "./loader.component";
import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";

@NgModule({
  imports: [
     CommonModule,
     IonicModule
  ],
  declarations: [
    LoaderComponent
  ],
  exports: [
    LoaderComponent,
  ]
})

export class LoaderModule {}
