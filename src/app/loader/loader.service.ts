import { EventEmitter } from "@angular/core";

export class LoaderService {
  public loading? : boolean;
  eventOcurred = new EventEmitter<boolean>();

  handleLoader(show : boolean) {
    this.loading = show;
    this.eventOcurred.emit(show);
  }
}
