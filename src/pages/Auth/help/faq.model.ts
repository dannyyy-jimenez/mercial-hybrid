import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

export class FAQ {
  param : string;
  title : string;
  answer : SafeResourceUrl;

  constructor(param : string, title : string, answer : SafeResourceUrl) {
    this.param = param;
    this.title = title;
    this.answer = answer;
  }
}
