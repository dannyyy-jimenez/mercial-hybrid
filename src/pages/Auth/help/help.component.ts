import { Component } from "@angular/core";
import { FAQ } from "./faq.model";
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { NavParams, ModalController } from "ionic-angular";
import { MessengerComponent } from "./messenger/messenger.component";
import { HelperViewerService } from "./viewer/viewer.helper.service";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "app-help",
  templateUrl: "./help.component.html"
})
export class HelpComponent {
  fragment : string;
  faqsBackup : FAQ[] = [];
  faqs : FAQ[] = [];

  constructor(private navParams: NavParams, private sanitizer: DomSanitizer, private modalController : ModalController, private api : APIService, private loaderService : LoaderService, private helperViewerService : HelperViewerService) {}

  ionViewWillEnter() {
    this.api.helpData().subscribe(
      (response : any) => {
        this.faqs = [];
        this.faqsBackup = [];
        const res = response._body._d;
        for (let faq of res._f) {
          this.faqsBackup.push(new FAQ(faq.param, faq.title, this.sanitizer.bypassSecurityTrustHtml(faq.answer)))
          this.faqs.push(new FAQ(faq.param, faq.title, this.sanitizer.bypassSecurityTrustHtml(faq.answer)))
        }
        if (this.fragment) {
          setTimeout(() => {
            this.scrollToFragment();
          }, 1000);
        }
        this.loaderService.handleLoader(false);
      }
    );
    this.fragment = this.navParams.get("fragment");
    this.scrollToFragment();
  }

  onSearch($event) {
    this.faqs = this.faqsBackup.filter((faq) => faq.title.toLowerCase().includes($event.target.value.toLowerCase()) || faq.answer.toString().toLowerCase().includes($event.target.value.toLowerCase()));
  }

  onMessageOpen() {
    let messageModal = this.modalController.create(MessengerComponent);
    messageModal.present();
  }

  scrollToFragment() {
    const element = document.getElementById(this.fragment);
    if (!element) {
      return;
    }
    element.scrollIntoView(true);
  }
}
