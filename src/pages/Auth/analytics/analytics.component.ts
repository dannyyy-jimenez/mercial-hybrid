import { Component } from "@angular/core";
import 'chart.js';
import 'chartjs-plugin-zoom'
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { TranslateService } from "@ngx-translate/core";
import { AuthService } from "../../../app/auth/auth.service";

@Component({
  selector: "app-analytics",
  templateUrl: './analytics.component.html'
})
export class AnalyticsComponent {
  pieChartLabels:string[] = [];
  pieChartData:number[] = [];
  pieChartType:string = 'pie';
  public pieChartColors: Array < any > = [{
   backgroundColor: [],
   borderColor: []
  }];

  public monthlyData:Array<any> = [
    {data: [], label : "Offers Redeemed", hidden : true},
    {data: [], label : "Mercials Watched", hidden : true},
    {data: [], label : "Both"}
  ];
  public secondsMonthlyData:Array<any> = [
    {data: []}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public lineChartOptions:any = {
    responsive: true,
    scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                beginAtZero: true   // minimum value will be 0.
            }
        }]
    },
        // Container for pan options
    pan: {
      // Boolean to enable panning
      enabled: true,

      // Panning directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow panning in the y direction
      mode: 'y',
      rangeMin: {
        // Format of min pan range depends on scale type
        x: null,
        y: 0
      },
      rangeMax: {
        // Format of max pan range depends on scale type
        x: null,
        y: null
      }
    },

    // Container for zoom options
    zoom: {
      // Boolean to enable zooming
      enabled: true,

      // Enable drag-to-zoom behavior
      drag: false,

      // Zooming directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow zooming in the y direction
      mode: 'y',
      rangeMin: {
        // Format of min zoom range depends on scale type
        x: null,
        y: 0
      },
      rangeMax: {
        // Format of max zoom range depends on scale type
        x: null,
        y: null
      }
    }
  };
  public lineChartColors:Array<any> = [
    {
      backgroundColor: 'rgba(102,51,153,0.2)',
      borderColor: 'rgb(102,51,153)',
      pointBackgroundColor: 'rgb(102,51,153)',
      pointBorderColor: 'rgb(102,51,153)',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(102,51,153)'
    }
  ];
  monthlyDataColors:Array<any> = [];
  public lineChartLegend:boolean = false;
  public lineChartType:string = 'line';

  merchipsEarned : number;
  moneySaved : number;
  offersRedeemed : number;

  constructor(private api : APIService, private authService: AuthService, private loaderService : LoaderService, private translate : TranslateService) {
    this.translate.get("months").subscribe((res : string[]) => {
      this.lineChartLabels = res;
    });
    this.translate.get("offers_redeemed").subscribe((res:string)=>{
      this.monthlyData[0].label = res;
    });
    this.translate.get("mercials_watched").subscribe((res:string)=>{
      this.monthlyData[1].label = res;
    });
    this.translate.get("both").subscribe((res:string)=>{
      this.monthlyData[2].label = res;
    });
  }

  ionViewWillEnter() {
    this.loaderService.handleLoader(true);
    this.api.analyticsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.merchipsEarned = res._me;
        this.moneySaved = res._ms;
        this.offersRedeemed = res._or;
        this.pieChartLabels = res._z.labels;
        this.pieChartData = res._z.data;
        this.pieChartColors[0].backgroundColor = res._z.colors.fill;
        this.pieChartColors[0].borderColor = res._z.colors.borders;
        this.monthlyData[0].data = res._md._or;
        this.monthlyData[1].data = res._md._mw;
        this.monthlyData[2].data = res._md._mo;
        this.monthlyDataColors = res._md.colors;
        this.secondsMonthlyData[0].data = res._smd;
        this.authService.setMerchips(res._mm);
        
        this.loaderService.handleLoader(false);
      }
    );
  }

  onRefresh($event) {
    this.api.analyticsData(() => $event.complete()).subscribe(
      (response : any) => {
        const res = response._body._d;
        this.merchipsEarned = res._me;
        this.moneySaved = res._ms;
        this.offersRedeemed = res._or;
        this.pieChartLabels = res._z.labels;
        this.pieChartData = res._z.data;
        this.pieChartColors[0].backgroundColor = res._z.colors.fill;
        this.pieChartColors[0].borderColor = res._z.colors.borders;
        this.monthlyData[0].data = res._md._or;
        this.monthlyData[1].data = res._md._mw;
        this.monthlyData[2].data = res._md._mo;
        this.monthlyDataColors = res._md.colors;
        this.secondsMonthlyData[0].data = res._smd;
        $event.complete();
      }
    );
  }
}
