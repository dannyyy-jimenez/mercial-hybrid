import { Component, OnInit, ViewChild, Input, Output, EventEmitter, AfterViewInit } from "@angular/core";
import { Offer } from "../offer/offer.model";
import { Title, DomSanitizer } from "@angular/platform-browser";
import { AuthService } from "../../../../app/auth/auth.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { APIService } from "../../../../app/auth/api.service";
import { NavParams, ModalController, ViewController } from "ionic-angular";
import { Socket } from 'ng-socket-io';
import { LaunchNavigator } from "@ionic-native/launch-navigator";
import { OfferViewerService } from "../../offers/redeem/viewer.offer.service";

@Component({
  selector: 'redeem-viewer',
  templateUrl: './redeem.component.html'
})
export class RedeemComponent implements AfterViewInit {
  @Input() offer : Offer;
  @Output('hide') hideViewer : EventEmitter<boolean> = new EventEmitter<boolean>();
  redeemed : boolean = false;
  stars : object = [1,2,3,4,5,6,7,8,9,10];
  currentStar : number;
  zipcodeNeeded : boolean = false;
  zipcodeEntered : boolean = false;
  rated : boolean = false;
  selectedLocation : string = null;
  rid : string = null;
  merchipsNeeded : number;
  showCode : boolean = false;
  fastRedeemCode = null;

  constructor(private titleService : Title, private sanitizer: DomSanitizer, private socket : Socket, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService, private api : APIService, private modalCtrl: ModalController, private launchNavigator: LaunchNavigator, private offerViewerService : OfferViewerService) {

  }

  ngAfterViewInit() {
    if (!this.offer) {
      return this.onHide();
    }
    document.getElementsByClassName('toolbar-background')[0].setAttribute('style', 'background: rgba(0,0,0,0.4) !important');
    document.getElementsByClassName('tabbar')[0].setAttribute('style', 'background: rgba(0,0,0,0.1) !important');
    this.zipcodeNeeded = this.offer.locations.length > 1;
    this.merchipsNeeded = this.offer.merchips - this.authService.merchips
    this.listen();
  }

  listen() {
    this.socket.on(`${this.authService.getSocket()}-${this.offer.id}.redeemed`, (data) => {
      if (data._s) {
        this.redeemed = true;
        this.zipcodeNeeded = data._e._z;
        this.authService.setMerchips(data._e._m);
      }
    });
  }

  onLocationSelect(locationSelector : any) {
    this.selectedLocation = this.offer.locations[locationSelector.selectedIndex - 1].formatted;
    this.api.enterZipcode({id : this.offer.id, rid : this.rid, location : this.offer.locations[locationSelector.selectedIndex - 1], auth : this.authService.getAUTH()}).subscribe(
      (response : any) => {
        const res = response._body._d;
        if (!res.z) {
          this.snackbarService.handleError({message : "invalid_loc", isError : true});
          return;
        }
        this.zipcodeEntered = true;
      }
    );
  }

  onRated() {
    this.api.rateFranchise({id : this.offer.id, rating : this.currentStar, auth : this.authService.getAUTH()}).subscribe(
      (response : any) => {
        const res = response._body._d;
        if (!res.r) {
          this.snackbarService.handleError({message : "invalid_rating", isError : true});
          return;
        }
        this.rated = true;
      }
    );
  }

  onGetFastQR() {
    this.api.fastQR({id: this.offer.id, auth: this.authService.getAUTH()}).subscribe((response: any) => {
      this.fastRedeemCode = this.sanitizer.bypassSecurityTrustHtml(response);
    });
  }

  onHide() {
    this.hideViewer.emit(true);
    this.offerViewerService.triggerViewer(false);
    document.getElementsByClassName('toolbar-background')[0].removeAttribute('style');
    document.getElementsByClassName('tabbar')[0].removeAttribute('style');
  }
}
