import { Offer } from "./offer/offer.model";
import { Injectable } from "@angular/core";
import { LoaderService } from "../../../app/loader/loader.service";
import { APIService } from "../../../app/auth/api.service";

@Injectable()
export class OfferService {
  private offers : Offer[] = [];
  private userZipcode : any;
  public previousPopulations: number = 1;
  public filters : any = {
    date : "newest",
    meta : null,
    location : "five"
  };

  constructor(private api : APIService, private loaderService : LoaderService) {}

  requestOffers(callback : Function, onError? : Function) {
    this.api.offersData(0, this.filters, onError).subscribe(
      (response : any) => {
        this.offers = [];
        const res = response._body._d;
        this.userZipcode = res._z;
        for (let offer of res._o) {
          this.offers.push(new Offer(offer.id, offer.franchise, offer.name, offer.image, offer.locations, offer.website, offer.redeem_code, offer.redeem_verify_code, offer.redeemable, offer.merchips, offer.savings, offer.description));
        }
        this.loaderService.handleLoader(false);
        callback();
      }
    );
  }

  olderFeed(callback: Function, onError?: Function) {
    this.api.offersData(this.previousPopulations, this.filters, onError).subscribe(
      (response: any) => {
        const res = response._body._d;
        let prevArr: Offer[] = [];
        for (let offer of res._o) {
          prevArr.push(new Offer(offer.id, offer.franchise, offer.name, offer.image, offer.locations, offer.website, offer.redeem_code, offer.redeem_verify_code, offer.redeemable, offer.merchips, offer.savings, offer.description));
        }
        this.previousPopulations++;
        this.loaderService.handleLoader(false);
        callback(prevArr);
      }, onError()
    );
  }

  getOffers() {
    return this.offers.slice();
  }

  public getZipcode() {
    return this.userZipcode;
  }

  getOffer(id:string) {
    return this.offers.find(offer => offer.id === id);
  }
}
