import { Component } from "@angular/core";
import { OfferService } from "./offers.service";
import { Offer } from "./offer/offer.model";
import { AlertController, Events } from "ionic-angular";
import { LoaderService } from "../../../app/loader/loader.service";
import { OfferViewerService } from "./redeem/viewer.offer.service";

@Component({
  selector: "mercial-offers",
  templateUrl: "./offers.component.html",
  providers: [OfferService]
})
export class OffersComponent {
  offers : Offer[] = [];
  keepLoading = true;
  offerAvailable : boolean = false;
  currentOffer : Offer = null;
  userZipcode : any;
  filterOpen = false;

  constructor(private offerService : OfferService, private loaderService : LoaderService, public alertCtrl: AlertController, private offerViewerService : OfferViewerService, private events : Events) {
    this.events.subscribe('offerFilterOpen', () => {
      this.filterOpen = true;
      document.getElementsByClassName('toolbar-background')[0].setAttribute('style', 'background: rgba(0,0,0,0.4) !important');
      document.getElementsByClassName('tabbar')[0].setAttribute('style', 'display: none');
      setTimeout(() => {
        document.getElementById('filter-container').className += " active";
      }, 50);
    });
  }

  ionViewWillEnter() {
    this.loaderService.handleLoader(true);
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
      this.offerService.previousPopulations = 1;
      this.userZipcode = this.offerService.getZipcode();
    }, () => this.loaderService.handleLoader(false));
  }

  onRefresh($event) {
    this.offers = [];
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
      this.offerService.previousPopulations = 1;
      $event.complete();
    }, () => $event.complete());
  }

  showOffer($event : Offer) {
    this.currentOffer = $event;
    this.offerAvailable = true;
    this.offerViewerService.triggerViewer(true);
  }

  public clearOffer($event) {
    this.offerAvailable = false;
    this.currentOffer = null;
  }

  onFilterChange() {
    this.offers = [];
    this.offerService.previousPopulations = 1;
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
    });
    this.keepLoading = true;
  }

  onFilterClose() {
    document.getElementById('filter-container').className = "filter-container";
    setTimeout(() => {
      document.getElementsByClassName('toolbar-background')[0].removeAttribute('style');
      document.getElementsByClassName('tabbar')[0].removeAttribute('style');
      this.filterOpen = false;
    }, 300);
  }

  populateOlderFeed() : Promise<any> {
    return new Promise((resolve) => {
      this.offerService.olderFeed((olderFeed : Offer[]) => {
        if (olderFeed.length == 0) {
          this.keepLoading = false;
        }
        this.offers = [...this.offers,...olderFeed];
        resolve();
      }, () => resolve());
    });
  }
}
