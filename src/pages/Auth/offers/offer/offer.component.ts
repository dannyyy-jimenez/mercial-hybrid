import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Offer } from "./offer.model";
import { ModalController, AlertController } from "ionic-angular";
import { RedeemComponent } from "../redeem/redeem.component";
import { AuthService } from "../../../../app/auth/auth.service";
import { APIService } from "../../../../app/auth/api.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { LaunchNavigator, LaunchNavigatorOptions } from "@ionic-native/launch-navigator";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'mercial-offer',
  templateUrl: './offer.component.html',
  styles: [
    `
      .addresses {
        z-index : 10000;
      }
    `
  ]
})
export class OfferComponent implements OnInit {
  @Input() offer : Offer;
  @Input() userLat : string;
  @Input() userLng : string;
  @Output() selected : EventEmitter<Offer> = new EventEmitter<Offer>();
  reportReason : "";
  reportExpand : "";
  hideRedeem : boolean = true;
  reasons = [
    { value : "unexistent", formatted : "Unexistent - The franchise owner is not a member" },
    { value : "fake", formatted : "Fake - Franchise owner scanned my code but did not give me the offer" },
  ];
  translationStrings : string[] = [];

  constructor(public modalCtrl: ModalController, private alertCtrl: AlertController, private authService : AuthService, private api : APIService, private snackbarService : SnackbarService, private launchNavigator: LaunchNavigator, private translate : TranslateService) {

  }

  ngOnInit() {
    this.translate.get("reportOffer").subscribe((res : string[]) => {
      if (!Array.isArray(res)) return;
      for (let i = 0; i < res.length; i++) {
        this.reasons[i].formatted = res[i];
      }
    });
    this.translate.get("reasonReport").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("cancel").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("continue").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("expandReport").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("reportOptional").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("feedback$dots").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("submit").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("locations").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("ok").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
  }

  onReport(myEvent) {
      let reasonAlert = this.alertCtrl.create();
      reasonAlert.setTitle(this.translationStrings[0]);

      for (let reason of this.reasons) {
        reasonAlert.addInput({
          type: 'radio',
          label: reason.formatted,
          value: reason.value,
          checked: false
        });
      }

      reasonAlert.addButton(this.translationStrings[1]);
      reasonAlert.addButton({
        text: this.translationStrings[2],
        handler: data => {
          this.reportReason = data;
          setTimeout(() => {
            const prompt = this.alertCtrl.create({
              title: this.translationStrings[3],
              message: this.translationStrings[4],
              inputs: [
                {
                  name: 'expand',
                  placeholder: this.translationStrings[5]
                },
              ],
              buttons: [
                {
                  text: this.translationStrings[1],
                  handler: data => {

                  }
                },
                {
                  text: this.translationStrings[6],
                  handler: data => {
                    this.reportExpand = data.expand;
                    this.onReportSubmit();
                  }
                }
              ]
          });
          prompt.present();
          }, 400);
        }
      });
      reasonAlert.present();
   }

   onReportSubmit() {
     if (this.reportReason == "") {
       return;
     }

     let report = {
       auth : this.authService.getAUTH(),
       id : this.offer.id,
       type : 'offer',
       reason : this.reportReason,
       expand : this.reportExpand
     };

     this.api.report(report).subscribe(
       (response : any) => {
         const res = response._body;

         if (res._m == "_invalidreason") {
           this.snackbarService.handleError({"message" : "invalid_reason", "isError" : false});
           return;
         }

         if (res._m == "_invalidid") {
           this.snackbarService.handleError({"message" : "video_offer_unavailable", "isError" : false});
           return;
         }

         this.snackbarService.handleError({"message" : "report_submitted", "isError" : false});
       }
     );
   }

  onAddressView() {
    let alert = this.alertCtrl.create({
      title: this.translationStrings[7],
      message: this.offer.locations.slice().map(location => location.formatted).join("<br/><br/>"),
      buttons: [this.translationStrings[8]]
    });
    alert.present();
  }

  onNavigation() {
    let options: LaunchNavigatorOptions = {
      start: [parseFloat(this.userLat), parseFloat(this.userLng)],
    }
    this.launchNavigator.navigate([this.offer.locations[0].lat, this.offer.locations[0].lng], options).then(
      success => {},
      error => console.log('Error launching navigator', error)
    );
  }

  onView() {
    this.selected.emit(this.offer);
  }
}
