export class Offer {
  id : string;
  franchise : any;
  name : string;
  description? : string;
  image : string;
  locations : any[];
  website : string;
  redeem_code : string;
  redeem_verify_code : string;
  redeemable : boolean;
  merchips : number;
  savings : string;
  hiddenCode : string;

  constructor(id : string, franchise : any,  name : string, image : string, locations : Array<{formatted : string, lat : number, lng : number}>, website : string, redeem_code : string, redeem_verify_code : string, redeemable: boolean, merchips : number, savings : string, description? : string) {
    this.id = id;
    this.franchise = franchise;
    this.name = name;
    this.description = description;
    this.image = image;
    this.locations = locations;
    this.website = website;
    this.redeem_code = redeem_code;
    this.redeem_verify_code = redeem_verify_code;
    this.redeemable = redeemable;
    this.merchips = merchips;
    this.savings = savings;
    this.hiddenCode = this.generateHidden();
  }

  generateHidden() {
    let code = "";
    for (let i = 0; i < this.redeem_verify_code.length; i++) {
      code += "&bull;";
    }
    return code;
  }

  public getLocations() {
    return this.locations.slice().map(location => location.formatted).join(", ")
  }

  public getCloudinaryID() {
    return this.redeem_code.match(/upload\/v[0-9]*\/(.*)\.(svg|png)/)[1];
  }
}
