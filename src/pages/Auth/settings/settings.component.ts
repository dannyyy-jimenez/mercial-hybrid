import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { APIService } from "../../../app/auth/api.service";
import { HelpComponent } from "../help/help.component";
import { AuthService } from "../../../app/auth/auth.service";
import { NoAuthComponent } from "../../NoAuth/noauth.component";
import { App, NavController, Tabs, Select } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import { LegalViewerComponent } from "../../Legal/legal.viewer.component";

@Component({
  selector: "app-settings",
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  @ViewChild('legalSelect') legalSelect: Select;
  passwordDisplay : string = "password";
  deletionIsValid : boolean = false;
  accountEmail : string;
  accountForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    email : new FormControl({value: '', disabled: false}, { validators: [Validators.email, Validators.required], updateOn: 'blur' }),
    password : new FormControl('')
  });
  targetsForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    ethnicity : new FormControl(''),
    languages : new FormControl([])
  });
  locationForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    zipcode : new FormControl(''),
  });
  preferencesForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    tags : new FormControl(''),
  });
  ethnicities = [
    { value : "asian/pacisl", init : false, formatted : "Asian / Pacific Islander" },
    { value : "black/afriame", init : false, formatted : "Black or African American" },
    { value : "hispanic/latino", init : false, formatted : "Hispanic or Latino" },
    { value : "natamer/ameind", init : false, formatted : "Native American or American Indian" },
    { value : "other", init : false, formatted : "Other" },
    { value : "white", init : true, formatted : "White" }
  ];
  legalDocs = [
    {value : 'clientGuide', formatted: "Guidelines"},
    {value : 'privacy-policy', formatted: "Privacy Policy"},
    {value : 'terms-of-use', formatted: "Terms of Service"},
    {value: 'dmca', formatted: "DMCA"},
    {value : 'disclaimer', formatted: "Disclaimer"}
  ];
  languages = [
    { value : "en", formatted : "English"},
    { value : "es", formatted : "Español"},
    { value : "fr", formatted : "Français"}
  ];
  // privacyForm = new FormGroup({
  //   auth : new FormControl(this.api.AUTH()),
  //   notif : new FormControl(false)
  // });
  deleteAccountForm = new FormGroup({
    auth : new FormControl(this.api.AUTH()),
    email : new FormControl({value : '', disabled : false}, [Validators.email, Validators.required]),
  });
  Help = HelpComponent;

  constructor(private api : APIService, private app : App, private nav: NavController, private authService : AuthService, private loaderService : LoaderService, private snackbarService : SnackbarService, private translate : TranslateService) {}

  ngOnInit() {
    this.translate.get("ethnicities$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.ethnicities[i].formatted = res[i];
      }
    });
    this.translate.get("legal$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.legalDocs[i].formatted = res[i];
      }
    });

    this.api.settingsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.accountForm.controls.email.setValue(res._a.email);
        this.locationForm.controls.zipcode.setValue(res._l.zipcode);
        this.preferencesForm.controls.tags.setValue(res._p.tags.join(", "));
        this.targetsForm.controls.ethnicity.setValue(res._t.eth);
        this.targetsForm.controls.languages.setValue(res._t.langs);
        if (res._t.eth !== "") {
          this.targetsForm.controls.ethnicity.disable();
        }
        // this.privacyForm.controls.notif.setValue(res._py.notif);
        this.accountEmail = res._a.email;
        this.loaderService.handleLoader(false);
      }
    )
  }

  togglePassword(hide : boolean) {
    this.passwordDisplay = hide ? 'password' : 'text';
  }

  validateDeletion() {
    const valid = this.deleteAccountForm.value.email.toLowerCase() === this.accountEmail;
    this.deletionIsValid = valid;
  }

  onAccountSubmit() {
    if (this.accountForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateAccount(this.accountForm.value).subscribe(
      (response : any) => {
        const res = response._body;

        if (res._m === "_emailinuse") {
          this.snackbarService.handleError({"message" : "email_inuse", "isError" : true});
          this.accountForm.controls.email.setErrors({'inuse':true});
        } else if (res._m === "_passshort") {
          this.snackbarService.handleError({"message" : "pass_len_error", "isError" : true});
          this.accountForm.controls.email.setErrors({'tooshort':true});
        } else {
          this.snackbarService.handleError({"message" : "account_updated", "isError" : false})
          this.accountForm.reset(this.accountForm.value);
        }
        this.loaderService.handleLoader(false);
      }
    );
  }

  onTargetSubmit() {
    if (this.targetsForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateTarget(this.targetsForm.value).subscribe(
      (response : any) => {
        const res = response._body;
        this.snackbarService.handleError({"message" : "account_updated", "isError" : false})
        this.loaderService.handleLoader(false);
      }
    );
  }

  onLocationSubmit() {
    if (this.locationForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateLocation(this.locationForm.value).subscribe(
      (response : any) => {
        this.snackbarService.handleError({"message" : "location_updated", "isError" : false})
        this.locationForm.reset(this.locationForm.value);
        this.loaderService.handleLoader(false);
      }
    );
  }

  onPreferencesSubmit() {
    if (this.preferencesForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updatePreferences(this.preferencesForm.value).subscribe(
      (response : any) => {
        const res = response._body;
        this.preferencesForm.controls.tags.setValue(res._f.join(", "));
        this.snackbarService.handleError({"message" : "preferences_updated", "isError" : false})
        this.preferencesForm.reset(this.preferencesForm.value);
        this.loaderService.handleLoader(false);
      }
    );
  }

  // onPrivacySubmit() {
  //   if (this.privacyForm.invalid) {
  //     return;
  //   }
  //   this.loaderService.handleLoader(true);
  //   this.api.updatePrivacy(this.privacyForm.value).subscribe(
  //     (response : any) => {
  //       this.snackbarService.handleError({"message" : "privacy_updated", "isError" : false})
  //       this.privacyForm.reset(this.privacyForm.value);
  //       this.loaderService.handleLoader(false);
  //     }
  //   );
  // }

  onLegalView($event) {
    if ($event.length === 0) return;
    this.app.getRootNav().push(LegalViewerComponent, {type: $event});
    this.legalSelect.setValue(null);
  }

  onDeleteSubmit() {
    if (this.deleteAccountForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.deleteAccount(this.deleteAccountForm.value).subscribe(
      () => {
        this.snackbarService.handleError({"message" : "account_deleted", "isError" : false})
        // this.privacyForm.reset(this.deleteAccountForm.value);
        this.authService.logoutUser();
        this.loaderService.handleLoader(false);
      }
    );
  }
}
