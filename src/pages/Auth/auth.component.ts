import { Component, ViewChild } from "@angular/core";
import { Tabs, NavController, App, NavParams, Events } from "ionic-angular";
import { AnalyticsComponent } from "./analytics/analytics.component";
import { FeedComponent } from "./feed/feed.component";
import { OffersComponent } from "./offers/offers.component";
import { HelpComponent } from "./help/help.component";
import { SettingsComponent } from "./settings/settings.component";
import { Subscription } from "rxjs";
import { AuthService } from "../../app/auth/auth.service";
import { HelperViewerService } from "./help/viewer/viewer.helper.service";
import { MercialViewerService } from "./feed/viewer/viewer.mercial.service";
import { OfferViewerService } from "./offers/redeem/viewer.offer.service";
import { NoAuthComponent } from "../NoAuth/noauth.component";

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html'
})
export class AuthComponent {
  @ViewChild('tabsRef') tabRef: Tabs;
  navController : NavController;
  Analytics = AnalyticsComponent;
  Feed = FeedComponent;
  Offers = OffersComponent;
  Help = HelpComponent;
  Settings = SettingsComponent;
  tabsOrder = [1,2,3];
  showHelper = false;
  helperURL : string = "";
  loaded:   boolean = false;
  tabIndex: number  = 0;
  subscription: Subscription;
  feedFilter : boolean = false;
  offerFilter : boolean = false;
  logOutEmition: Subscription;

  constructor(private app : App, private navParams : NavParams, private authService : AuthService, private helperViewerService : HelperViewerService, private mercialViewerService : MercialViewerService, private offerViewerService : OfferViewerService, private events : Events) {
    this.subscription = this.helperViewerService.getTrigger().subscribe((data) => { this.showHelper = data.show; this.helperURL = data.url });
    this.logOutEmition = this.authService.getSubcription().subscribe((logout) => {
      if (!logout) return;
      this.navController.push(NoAuthComponent).then(()=>{
        this.navController.remove(0,1);
      });
    });
  }

  ionViewDidEnter() {

  }

  ionViewDidLoad() {
    this.navController = this.app.getRootNav();
    var firstTime : boolean = this.navParams.get("firstTime");
    if (firstTime) {
      this.helperURL = "https://res.cloudinary.com/aurodim/video/upload/v1546473150/mercial/static/explainers/clients-beginner.mp4";
      this.showHelper = true;
    }
  }

  onLogout() {
    this.authService.logoutUser();
  }

  onSwipe($event) {
    if (this.offerViewerService.getShowing() || this.mercialViewerService.getShowing()) {
      return;
    }

    if ($event.overallVelocityX > 0) {
      let ind = this.tabRef.getSelected().index - 1;
      if (ind == -1) return;
      this.tabRef.select(ind);
    } else {
      let ind = this.tabRef.getSelected().index + 1;
      if (ind == 5) return;
      this.tabRef.select(ind);
    }
  }

  onTabChange($event) {
    this.feedFilter = false;
    this.offerFilter = false;
    if ($event.index == 1) {
      this.feedFilter = true;
    }
    if ($event.index == 2) {
      this.offerFilter = true;
    }

    if ($event.index == 0 || $event.index == 5) return;

    this.tabsOrder = [$event.index - 1, $event.index, $event.index + 1];

    if (!this.loaded) {
      this.loaded = true;
      return;
    }
  }

  onFeedFilterClick(myEvent) {
     this.events.publish('feedFilterOpen');
   }

   onOfferFilterClick(myEvent) {
    this.events.publish('offerFilterOpen');
  }

  ionViewCanEnter() {
      this.authService.isAuthenticated()
  }

  ionViewCanLeave() {
      !this.authService.isAuthenticated()
  }

  ngOnDestroy() {
    this.logOutEmition.unsubscribe();
    this.subscription.unsubscribe();
  }
}
