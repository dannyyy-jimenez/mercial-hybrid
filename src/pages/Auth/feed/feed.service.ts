import { Mercial } from "./mercial/mercial.model";
import { Injectable } from "@angular/core";
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";

@Injectable()
export class FeedService {
  private feed: Mercial[] = [];
  public previousPopulations: number = 1;
  public filters : any = {
    date : "newest",
    meta : null,
    categories : null
  };

  constructor(private api: APIService, private loaderService: LoaderService) { }

  requestFeed(callback: Function, onError?: Function) {
    this.api.feedData(0, this.filters, onError).subscribe(
      (response: any) => {
        this.feed = [];
        const res = response._body._d;
        for (let mercial of res._f) {
          this.feed.push(new Mercial(mercial.id, this.getPublisher(mercial.publisher), mercial.title, mercial.thumbnail, mercial.video, mercial.size, mercial.merchips, mercial.duration, mercial.tags, mercial.description, mercial.more, mercial.attributes))
        }
        this.loaderService.handleLoader(false);
        callback();
      }
    );
  }

  olderFeed(callback: Function, onError?: Function) {
    this.api.feedData(this.previousPopulations, this.filters, onError).subscribe(
      (response: any) => {
        const res = response._body._d;
        let prevArr: Mercial[] = [];
        for (let mercial of res._f) {
          prevArr.push(new Mercial(mercial.id, this.getPublisher(mercial.publisher), mercial.title, mercial.thumbnail, mercial.video, mercial.size, mercial.merchips, mercial.duration, mercial.tags, mercial.description, mercial.more, mercial.attributes))
        }
        this.previousPopulations++;
        this.loaderService.handleLoader(false);
        callback(prevArr);
      }
    );
  }

  getPublisher(publisher: string) {
    if (!publisher) return null;

    if (publisher.replace(/ /gi, "") == "") {
      return null;
    } else {
      return publisher;
    }
  }

  clearFeed() {
    this.feed = [];
  }

  getFeed() {
    return this.feed.slice();
  }

  getFeedItem(id: string) {
    return this.feed.find(ad => ad.id === id);
  }

  hasProperty(id: string, property: string) {
    const ad: Mercial = this.getFeedItem(id);
    return ad.attributes.indexOf(property) !== -1 ? true : false;
  }

  setFeed(feed: Mercial[]) {
    this.feed = feed;
  }
}
