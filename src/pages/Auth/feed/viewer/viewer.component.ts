import { Component, ViewChild, HostListener, Input, EventEmitter, Output, AfterViewInit, OnInit } from "@angular/core";
import { Mercial } from "../mercial/mercial.model";
import { Title } from "@angular/platform-browser";
import { NavParams, ModalController, ActionSheetController } from "ionic-angular";
import { AuthService } from "../../../../app/auth/auth.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { APIService } from "../../../../app/auth/api.service";
import { CacheSrcService } from 'ionic-cache-src';
import { Subscription } from "rxjs";
import { MercialViewerService } from "./viewer.mercial.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector : 'feed-viewer',
  templateUrl: './viewer.component.html'
})
export class ViewerComponent implements OnInit, AfterViewInit {
  @Input() mercial : Mercial;
  @Output('hide') hideViewer : EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('viewerPlayer') viewerPlayer: any;
  width : number;
  ad : Mercial;
  cancel = "Cancel";
  showControls : boolean = false;
  loadFailed : boolean = false;
  seconds : number = 0;
  left : number = 0;
  ended : boolean = false;
  watching : boolean = true;
  blurred : boolean = true;
  seeking : boolean = false;
  timeout;
  hideable : boolean = false;
  awardedMerchips : number = 0;
  interval;

  @HostListener('window:blur', ['$event'])
    onBlur(event: any): void {
      if (!this.loadFailed && !this.ended) {
        if (!this.viewerPlayer) return;
        this.onPause();
      }
    }

  constructor(private authService : AuthService, private actionSheetCtrl: ActionSheetController, private translatorService: TranslateService, private snackbarService : SnackbarService, private loaderService : LoaderService, private api : APIService, private _cacheSrv: CacheSrcService, private mercialViewerService : MercialViewerService) {}

  ngOnInit() {
    this.width = window.innerWidth-20;
    this.ad = this.mercial;
    this.translatorService.get("cancel").subscribe(str => {
      this.cancel = str
    });
    document.getElementsByClassName('toolbar-background')[0].setAttribute('style', 'background: rgba(0,0,0,0.4) !important');
    document.getElementsByClassName('tabbar')[0].setAttribute('style', 'background: rgba(0,0,0,0.1) !important');
  }

  ngAfterViewInit() {
    // this.api.feedItem(this.mercial.id).subscribe(
    //   (response : any) => {
    //     const res = response._body._d;
    //     const mercial = res._i;
    //     if (document.getElementById('vh')) {
    //       document.getElementById('vh').style.height = document.getElementById('v').clientHeight > 300 ? document.getElementById('v').clientHeight + 'px' : '300px';
    //     }
    //     if (mercial != null) {
    //       this.ad = new Mercial(mercial.id, mercial.publisher, mercial.title, mercial.thumbnail, mercial.video, mercial.size, mercial.merchips, mercial.duration, mercial.tags, mercial.description, mercial.more, mercial.attributes);
    //       this.playVideo(null);
    //     } else {
    //       setTimeout(()=>{
    //         this.snackbarService.handleError({message : "inexistent_video", isError : true});
    //         this.onHide();
    //       }, 2000);
    //     }
    //   }
    // );
  }

  onLoad() {
    this.loaderService.handleLoader(false);
    if (document.getElementById('vh')) {
      document.getElementById('vh').style.height = document.getElementById('v').clientHeight > 300 ? document.getElementById('v').clientHeight + 'px' : '300px';
    }
    this.playVideo(null);
  }

  hasProperty(property:string) {
    if (!this.ad) return false;
    return this.ad.attributes.indexOf(property) !== -1 ? true :false;
  }

  formatTime(time) {
    let format = ["0", "0", ":", "0", "0"];
    if (time > 60) {
      format[0] = Math.floor((time / 60 / 10)).toFixed(0);
      format[1] = Math.floor(((time / 60) % 10)).toFixed(0);
    }
    format[3] = Math.floor((time % 60 / 10)).toFixed(0);
    format[4] = Math.floor(((time % 60) % 10)).toFixed(0);
    return format.join("");
  }


  onPause() {
    this.showControls = true;
    this.blurred = true;
    this.watching = false;
    if (!this.viewerPlayer) return;
    this.viewerPlayer.nativeElement.pause();
    if (document.getElementById("tks")) {
      document.getElementById("tks").style.animationPlayState = "paused";
    }
  }

  onVideoOptions() {
    this.showControls = true;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (this.showControls && !this.blurred) {
        this.showControls = false;
      }
    }, 2000);
  }

  playVideo(event: any) {
    if (this.loadFailed) {
      this.onError();
      return;
    }
    this.loaderService.handleLoader(false);
    this.blurred = false;
    if (!this.viewerPlayer) return;
    if (!this.timeout) {
      this.showControls = false;
    }
    this.viewerPlayer.nativeElement.play();
    if (document.getElementById("tks")) {
      document.getElementById("tks").style.animationPlayState = "running";
      document.getElementById("tks").style.animationDuration = Math.ceil(this.ad.duration)+"s";
    }
    this.watching = true;
    this.interval = setInterval(() => {
      if (!this.viewerPlayer) return;
      this.seconds = this.viewerPlayer.nativeElement.currentTime;
      this.left = Math.ceil(this.ad.duration) - (this.seconds + 0.3);
    }, 1000);
  }

  onSeeking($event) {
    if (!this.viewerPlayer) return;
    if (this.seconds < this.viewerPlayer.nativeElement.currentTime) {
      this.viewerPlayer.nativeElement.currentTime = this.seconds;
    }
  }

  onSeeked($event) {
    if (!this.viewerPlayer) return;
    if (this.seconds < this.viewerPlayer.nativeElement.currentTime) {
      this.viewerPlayer.nativeElement.currentTime = this.seconds;
    }
  }

  onTimeChange($event) {
    if (!this.viewerPlayer) return;
    if (this.blurred) {
      this.viewerPlayer.nativeElement.pause();
      return;
    }
    this.viewerPlayer.nativeElement.playbackRate = 1;
  }

  onEnded() {
    this.loaderService.handleLoader(true);
    this.api.mercialWatched({id : this.mercial.id, auth : this.authService.getAUTH()}).subscribe(
      (response : any) => {
        const res = response._body._d;
        if (response.m == "_unavailable") {
          this.snackbarService.handleError({message : "mercial_unavailable", isError : true});
        }
        this.awardedMerchips = res._mr;
        this.authService.setMerchips(res._ma);
        this.ended = true;
        this.loaderService.handleLoader(false);
      }
    );
  }

  onHide() {
    if (this.viewerPlayer) {
      this.viewerPlayer.nativeElement.pause();
      this.viewerPlayer.nativeElement.src = "";
    }
    clearInterval(this.interval);
    this.hideViewer.emit(true);
    this.mercialViewerService.triggerVideo(false);
    document.getElementsByClassName('toolbar-background')[0].removeAttribute('style');
    document.getElementsByClassName('tabbar')[0].removeAttribute('style');
  }

  onError() {
    this.loadFailed = true;
    this.snackbarService.handleError({message : "mercial_load_failed", isError : true});
  }

  onInformation() {
    let arr = [];
    if (this.mercial.more) {
      this.mercial.more.forEach(more => {
       let formatted : any = "";
       let after = [];
       let moreCopy = more;
       if (moreCopy.includes("://")) {
         moreCopy = moreCopy.substring(moreCopy.indexOf("://")+3);
         after = moreCopy.split("/");
         after  = after.map(a => a.toUpperCase());
       }
       formatted = moreCopy.split("/")[0].split(".");
       if (formatted.length == 2) {
         formatted = formatted[0].substring(0,1).toUpperCase() + formatted[0].substring(1);
       } else {
         formatted = formatted[1].substring(0,1).toUpperCase() + formatted[1].substring(1);
       }
       if (after.length > 1) {
         formatted += " - " + after.splice(1).join(" ");
       }
        arr.push({
          text: formatted,
          handler: () => {
            window.open(this.cleanMore(more),'_system', 'location=yes');
            this.api.addInterest({id : this.mercial.id, auth : this.authService.getAUTH()}).subscribe(
              (response : any) => {

              }
            );
            return false;
          }
        });
      });
    }

    arr.push({
      text: this.cancel,
      role: 'cancel',
      handler: () => {

      }
    });

   const actionSheet = this.actionSheetCtrl.create({
      title: this.mercial.description,
      buttons: arr
    });
    actionSheet.present();
 }

 cleanMore(url: string) {
   if (url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
     url = "http://" + url;
   }
   return url;
 }
}
