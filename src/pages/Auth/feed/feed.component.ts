import { Component, OnInit } from "@angular/core";
import { Mercial } from "./mercial/mercial.model";
import { FeedService } from "./feed.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { MercialViewerService } from "./viewer/viewer.mercial.service";
import { AlertController, NavParams, Events } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styles: [],
  providers : [FeedService]
})
export class FeedComponent implements OnInit {
  ads : Mercial[] = [];
  mercialAvailable : boolean = false;
  currentMercial : Mercial = null;
  keepLoading = true;
  filterOpen = false;
  categories = {};
  categoriesKeys = [];

  constructor(private feedService : FeedService, private loaderService : LoaderService, private mercialViewerService : MercialViewerService, public alertCtrl: AlertController, public events: Events, private translate : TranslateService) {
    this.events.subscribe('feedFilterOpen', () => {
      this.filterOpen = true;
      document.getElementsByClassName('toolbar-background')[0].setAttribute('style', 'background: rgba(0,0,0,0.4) !important');
      document.getElementsByClassName('tabbar')[0].setAttribute('style', 'display: none');
      setTimeout(() => {
        document.getElementById('filter-container').className += " active";
      }, 50);
    });
  }

  ngOnInit() {
    this.translate.get("categoriesFilter").subscribe((res : string[]) => {
      this.categories = res;
      this.categoriesKeys = Object.keys(this.categories);
    });
  }

  ionViewWillEnter() {
    this.loaderService.handleLoader(true);
    this.feedService.requestFeed(() => {
      this.feedService.previousPopulations = 1;
      this.ads = [];
      this.ads = this.feedService.getFeed();
      this.loaderService.handleLoader(false);
    }, () => this.loaderService.handleLoader(false));
  }

  onRefresh($event) {
    this.ads = [];
    this.feedService.clearFeed();
    this.feedService.requestFeed(() => {
      this.ads = [];
      this.ads = this.feedService.getFeed();
      $event.complete();
      this.feedService.previousPopulations = 1;
    }, () => $event.complete());
    this.keepLoading = true;
  }

  onFilterChange() {
    this.feedService.previousPopulations = 1;
    this.feedService.requestFeed(() => {
      this.ads = [];
      this.ads = this.feedService.getFeed();
    });
    this.keepLoading = true;
  }

  onFilterClose() {
    document.getElementById('filter-container').className = "filter-container";
    setTimeout(() => {
      document.getElementsByClassName('toolbar-background')[0].removeAttribute('style');
      document.getElementsByClassName('tabbar')[0].removeAttribute('style');
      this.filterOpen = false;
    }, 300);
  }

  showMercial($event : Mercial) {
    this.currentMercial = $event;
    this.mercialAvailable = true;
    this.mercialViewerService.triggerVideo(true);
  }

  public clearMercial($event) {
    this.mercialAvailable = false;
    this.currentMercial = null;
  }

  onCategorySelect(e) {
    if (this.feedService.filters.categories && this.feedService.filters.categories.includes(e._item._elementRef.nativeElement.children[0].attributes.value.value)) {
      let arr = this.feedService.filters.categories.split(",");
      arr.splice(arr.indexOf(e._item._elementRef.nativeElement.children[0].attributes.value.value), 1);
      this.feedService.filters.categories = arr.join(",");
      if (this.feedService.filters.categories.length == 0) {
        this.feedService.filters.categories = null;
      }
    } else {
      if (this.feedService.filters.categories) {
        this.feedService.filters.categories += ","+e._item._elementRef.nativeElement.children[0].attributes.value.value;
      } else {
        this.feedService.filters.categories = e._item._elementRef.nativeElement.children[0].attributes.value.value;
      }
    }
  }

  populateOlderFeed() : Promise<any> {
    return new Promise((resolve) => {
      this.feedService.olderFeed((olderFeed : Mercial[]) => {
        if (olderFeed.length == 0) {
          this.keepLoading = false;
        }
        this.ads = [...this.ads,...olderFeed];
        resolve();
      }, () => resolve());
    });
  }
}
