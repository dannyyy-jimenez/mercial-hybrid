import { Component, Input, EventEmitter, Output, OnInit } from "@angular/core";
import { Mercial } from "./mercial.model";
import { ViewerComponent } from "../viewer/viewer.component";
import { ModalController, AlertController, ActionSheetController } from "ionic-angular";
import { APIService } from "../../../../app/auth/api.service";
import { AuthService } from "../../../../app/auth/auth.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'mercial-mercial',
  templateUrl: './mercial.component.html',
  styles: [`
    .tags {
      z-index: 100000;
    }
  `]
})
export class MercialComponent implements OnInit {
  @Input() mercial : Mercial;
  @Output() selected : EventEmitter<Mercial> = new EventEmitter<Mercial>();
  reportReason : "";
  reportExpand : "";
  reasons = [
      { value : "disturbing", formatted : "Disturbing content" },
      { value : "violent", formatted : "Violent or repulsive content" },
      { value : "hateful", formatted : "Hateful or abusive content" },
      { value : "terrorism", formatted : "Promotes terrorism" },
      { value : "infringement", formatted : "Infringes my rights" },
      { value : "restriction", formatted : "Violates age restriction" },
  ];
  translationStrings : string[] = [];

  constructor(private api : APIService, private authService : AuthService, private alertCtrl: AlertController, private actionSheetCtrl: ActionSheetController, private modalCtrl: ModalController, private snackbarService : SnackbarService, private translate : TranslateService) {

  }

  ngOnInit() {
    this.translate.get("reportMercial").subscribe((res : string[]) => {
      if (!Array.isArray(res)) return;
      for (let i = 0; i < res.length; i++) {
        this.reasons[i].formatted = res[i];
      }
    });
    this.translate.get("tags").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("reasonReport").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("cancel").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("continue").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("expandReport").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("reportOptional").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("feedback$dots").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("submit").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("ok").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
    this.translate.get("back").subscribe((res : string) => {
      this.translationStrings.push(res);
    });
  }

  onTagsClick(e) {
    e.preventDefault();
    e.stopPropagation();
    let alert = this.alertCtrl.create({
      title: this.translationStrings[0],
      message: "<br/>"+this.mercial.tags.slice().join("<br/><br/>"),
      buttons: [this.translationStrings[8]]
    });
    alert.present();
  }

  onView(e) {
    e.preventDefault();
    e.stopPropagation();
    this.selected.emit(this.mercial);
  }

  onReport(e) {
    e.preventDefault();
    e.stopPropagation();
    let reasonAlert = this.alertCtrl.create();
    reasonAlert.setTitle(this.translationStrings[1]);

    for (let reason of this.reasons) {
      reasonAlert.addInput({
        type: 'radio',
        label: reason.formatted,
        value: reason.value,
        checked: false
      });
    }

    reasonAlert.addButton(this.translationStrings[2]);
    reasonAlert.addButton({
      text: this.translationStrings[3],
      handler: data => {
        this.reportReason = data;
        setTimeout(() => {
          const prompt = this.alertCtrl.create({
            title: this.translationStrings[4],
            message: this.translationStrings[5],
            inputs: [
              {
                name: 'expand',
                placeholder: this.translationStrings[6]
              },
            ],
            buttons: [
              {
                text: this.translationStrings[1],
                handler: data => {

                }
              },
              {
                text: this.translationStrings[7],
                handler: data => {
                  this.reportExpand = data.expand;
                  this.onReportSubmit();
                }
              }
            ]
        });
        prompt.present();
        }, 400);
      }
    });
    reasonAlert.present();
 }

 onReportSubmit() {
   if (this.reportReason == "") {
     return;
   }

   let report = {
     auth : this.authService.getAUTH(),
     id : this.mercial.id,
     type : 'mercial',
     reason : this.reportReason,
     expand : this.reportExpand
   };

   this.api.report(report).subscribe(
     (response : any) => {
       const res = response._body;

       if (res._m == "_invalidreason") {
         this.snackbarService.handleError({"message" : "invalid_reason", "isError" : false});
         return;
       }

       if (res._m == "_invalidid") {
         this.snackbarService.handleError({"message" : "video_offer_unavailable", "isError" : false});
         return;
       }

       this.snackbarService.handleError({"message" : "report_submitted", "isError" : false});
     }
   );
 }

 onInformation(e) {
   e.preventDefault();
   e.stopPropagation();
   let arr = [];
   if (this.mercial.more) {
     this.mercial.more.forEach(more => {
      let formatted : any = "";
      let after = [];
      let moreCopy = more;
      if (moreCopy.includes("://")) {
        moreCopy = moreCopy.substring(moreCopy.indexOf("://")+3);
        after = moreCopy.split("/");
        after  = after.map(a => a.toUpperCase());
      }
      formatted = moreCopy.split("/")[0].split(".");
      if (formatted.length == 2) {
        formatted = formatted[0].substring(0,1).toUpperCase() + formatted[0].substring(1);
      } else {
        formatted = formatted[1].substring(0,1).toUpperCase() + formatted[1].substring(1);
      }
      if (after.length > 1) {
        formatted += " - " + after.splice(1).join(" ");
      }
       arr.push({
         text: formatted,
         handler: () => {
           window.open(this.cleanMore(more),'_system', 'location=yes');
           this.api.addInterest({id : this.mercial.id, auth : this.authService.getAUTH()}).subscribe(
             (response : any) => {

             }
           );
           return false;
         }
       });
     });
   }

   arr.push({
     text: this.translationStrings[9],
     role: 'cancel',
     handler: () => {

     }
   });

  const actionSheet = this.actionSheetCtrl.create({
     title: this.mercial.description,
     buttons: arr
   });
   actionSheet.present();
}

  cleanMore(url: string) {
    if (url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
      url = "http://" + url;
    }
    return url;
  }
}
