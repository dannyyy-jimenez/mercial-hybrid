import { Component, OnDestroy, OnInit } from "@angular/core";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { AuthService } from "../../../app/auth/auth.service";
import { APIService } from "../../../app/auth/api.service";
import { AlertController } from "ionic-angular";
import { Subscription } from "rxjs";
import { Socket } from "ng-socket-io";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'mercial-redeem-validator',
  templateUrl: './redeem-validator.component.html'
})
export class RedeemValidatorComponent implements OnInit, OnDestroy {
  awaiting : boolean = false;
  success : boolean = false;
  codeData : any;
  timeout : any;
  subscription : Subscription;
  confirm : any;
  sc : number;
  alertTrans = {
    title : "",
    message : "",
    buttons : ["", ""],
    handled : "",
    handled_alert : "",
    awesome : "",
    awesome_success : ""
  };

  constructor(private loaderService : LoaderService, private socket : Socket, private snackbarService : SnackbarService, private authService : AuthService, private api : APIService, private alertCtrl: AlertController, private translate : TranslateService) {
     this.translate.get("redeem_prompt").subscribe((res : string) => this.alertTrans.title = res);
     this.translate.get("redeem_alert").subscribe((res : string) => this.alertTrans.message = res);
     this.translate.get("no").subscribe((res : string) => this.alertTrans.buttons[0] = res)
     this.translate.get("yes").subscribe((res : string) => this.alertTrans.buttons[1] = res)
     this.translate.get("handled").subscribe((res : string) => this.alertTrans.handled = res)
     this.translate.get("handled_alert").subscribe((res : string) => this.alertTrans.handled_alert = res)
     this.translate.get("redeem_awesome").subscribe((res : string) => this.alertTrans.awesome = res)
     this.translate.get("redeem_success").subscribe((res : string) => this.alertTrans.awesome_success = res)
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.sc = Math.floor(Math.random() * 10000000000);
    this.socket.on(this.authService.getSocket(), (data) => {
      this.loaderService.handleLoader(true);
      this.awaiting = true;
      this.codeData = data;
      this.codeData._u = {
        _a : this.authService.getAUTH(),
        _s : this.authService.getSocket()+"-handled",
        _d : null,
        _sc : this.sc
      };

      this.confirm = this.alertCtrl.create({
        title: this.alertTrans.title,
        message: this.alertTrans.message,
        buttons: [
          {
            text: this.alertTrans.buttons[0],
            handler: () => this.onCancel()
          },
          {
            text: this.alertTrans.buttons[1],
            handler: () => this.onApprove()
          }
        ]
      });
      this.confirm.present();
    });
    this.socket.on(this.authService.getSocket()+"-handled", (data)=>{
      if (this.confirm) {
        this.confirm.dismiss();
      }
      if (data._e._sc !== this.sc) {
        const alert = this.alertCtrl.create({
           title: this.alertTrans.handled,
           subTitle: this.alertTrans.handled_alert,
           buttons: [
             {
               text: 'OK',
               handler: () => {
                 this.loaderService.handleLoader(false);
                 this.awaiting = false;
               }
             }
           ]
         });
         alert.present();
      }
    });
  }

  onMousedown(cb) {
    this.timeout = setTimeout(() => cb, 2000);
  }

  onMouseup(cb) {
    clearTimeout(this.timeout);
  }

  onCancel() {
    this.confirm = null;
    this.codeData._u._d = false;
    this.loaderService.handleLoader(true);
    this.api.validateRedeem({auth : this.authService.getAUTH(), codeData : this.codeData}, () => this.onError()).subscribe(
      (response : any) => {
        this.loaderService.handleLoader(false);
        this.awaiting = false;
      }
    );
  }

  onApprove() {
    this.confirm = null;
    this.codeData._u._d = true;
    this.loaderService.handleLoader(true);
    this.api.validateRedeem({auth : this.authService.getAUTH(), codeData : this.codeData}, () => this.onError()).subscribe(
      (response : any) => {
        let res = response._body._d;
        if (res._s) {
          this.authService.setMerchips(res._m);
          this.loaderService.handleLoader(false);
          this.awaiting = false;
          const alert = this.alertCtrl.create({
             title: this.alertTrans.awesome,
             subTitle: this.alertTrans.awesome_success,
             buttons: ['OK']
           });
           alert.present();
        } else {
          if (response._body._m === "_notenoughmerchips") {
            this.snackbarService.handleError({message : "merchips_error", isError : true});
          } else {
            this.snackbarService.handleError({message : "try_again", isError : true});
          }
          this.loaderService.handleLoader(false);
          this.awaiting = false;
          this.success = false;
        }
      }
    );
  }

  onError() {
    this.snackbarService.handleError({message : "try_again", isError : true});
    this.awaiting = false;
  }
}
