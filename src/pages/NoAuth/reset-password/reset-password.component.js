var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { LoginComponent } from "../login/login.component";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../../app/auth/auth.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { NavParams, Nav } from "ionic-angular";
import { APIService } from "../../../app/auth/api.service";
var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(navParams, nav, api, authService, snackbarService, loaderService) {
        this.navParams = navParams;
        this.nav = nav;
        this.api = api;
        this.authService = authService;
        this.snackbarService = snackbarService;
        this.loaderService = loaderService;
        this.login = LoginComponent;
        this.forgot = ForgotPasswordComponent;
        this.resetForm = new FormGroup({
            code: new FormControl('', { validators: [Validators.required] }),
            email: new FormControl('', { validators: [Validators.email, Validators.required] }),
            "new-password": new FormControl('', { validators: [Validators.required] }),
            "confirm-password": new FormControl('', { validators: [Validators.required] })
        });
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        if (this.navParams.get("code")) {
            this.resetForm.controls.code.setValue(this.navParams.get("code"));
            if (this.navParams.get("code").length < 6 || this.navParams.get("code").length > 8) {
                this.resetForm.controls.code.markAsDirty();
            }
        }
        if (this.navParams.get("email")) {
            this.resetForm.controls.email.setValue(this.navParams.get("email"));
        }
    };
    ResetPasswordComponent.prototype.mismatch = function () {
        if (this.resetForm.controls["new-password"].value !== this.resetForm.controls["confirm-password"].value) {
            this.resetForm.controls["confirm-password"].setErrors({ mismatch: true });
        }
    };
    ResetPasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.resetForm.invalid) {
            return;
        }
        this.loaderService.handleLoader(true);
        this.api.reset(this.resetForm.value).subscribe(function (response) {
            if (response._body._m === "_passshort") {
                _this.resetForm.controls['new-password'].setErrors({ 'invalid': true });
                _this.snackbarService.handleError({ "message": "pass_len_error", "isError": true });
            }
            else if (response._body._m === "_invalidcode") {
                _this.resetForm.controls['email'].setErrors({ 'invalid': true });
                _this.resetForm.controls['code'].setErrors({ 'invalid': true });
                _this.snackbarService.handleError({ "message": "email_code_mismatch", "isError": true });
            }
            else {
                _this.snackbarService.handleError({ "message": "pass_reset_success", "isError": false });
                _this.resetForm.reset();
            }
            _this.loaderService.handleLoader(false);
        }, function (error) {
            _this.snackbarService.handleError({ "message": error.status + " " + error.statusText, "isError": true, raw: true });
            _this.loaderService.handleLoader(false);
        });
    };
    ResetPasswordComponent.prototype.ionViewWillEnter = function () {
        this.nav.swipeBackEnabled = false;
    };
    ResetPasswordComponent.prototype.ionViewWillLeave = function () {
        this.nav.swipeBackEnabled = true;
    };
    ResetPasswordComponent = __decorate([
        Component({
            selector: 'app-reset-password',
            templateUrl: 'reset-password.component.html'
        }),
        __metadata("design:paramtypes", [NavParams, Nav, APIService, AuthService, SnackbarService, LoaderService])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());
export { ResetPasswordComponent };
//# sourceMappingURL=reset-password.component.js.map