import { Component, ViewChild, OnInit, ElementRef } from "@angular/core";
import { Slides, NavController } from "ionic-angular";
import { RegisterComponent } from "../register/register.component";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-landing',
  templateUrl: 'landing.component.html'
})
export class LandingComponent implements OnInit {
  @ViewChild(Slides) slides: Slides;
  videoURL : string;
  videoPoster : string;
  startFade : boolean = false;

  constructor(public navCtrl: NavController, private translator: TranslateService) {}

  ngOnInit() {
    var element = document.getElementById("slides");
  }

  ionViewDidLoad() {
    this.videoPoster = `https://res.cloudinary.com/aurodim/video/upload/w_${(window.innerWidth * 0.95).toFixed(0)}/mercial/static/explainers/clients-explainer.png`;
    this.videoURL = `https://res.cloudinary.com/aurodim/video/upload/w_${(window.innerWidth * 0.95).toFixed(0)}/mercial/static/explainers/clients-explainer.mp4`;
  }

  onSkipClick() {
    this.navCtrl.push(RegisterComponent);
    this.navCtrl.remove(0);
  }

  onContinueClick() {
    this.navCtrl.push(RegisterComponent);
    this.navCtrl.remove(0);
  }
}
