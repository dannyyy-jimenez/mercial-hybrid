import { Component } from "@angular/core";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { RegisterComponent } from "../register/register.component";
import { NavController, App } from "ionic-angular";
import { NgForm } from "@angular/forms";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { AuthComponent } from "../../Auth/auth.component";
import * as auth from '../../../app/auth/auth.barrel';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  register = RegisterComponent;
  forgot = ForgotPasswordComponent;

  constructor(public navCtrl: NavController, private app : App, private authService : auth.AuthService, private api : auth.APIService, private snackbarService : SnackbarService, private loaderService : LoaderService) { }

  onLogin(loginForm : NgForm) {
    if (loginForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.loginUser(loginForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_emailinvalid") {
          loginForm.controls['email'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "email_unrecognized", "isError" : true});
        } else if (response._body._m === "_passinvalid") {
          loginForm.controls['password'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "incorrect_password", "isError" : true});
        } else if (response._body._m === "_accountlocked") {
          loginForm.controls['email'].setErrors({'invalid' : true});
          loginForm.controls['password'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "account_locked", "isError" : true});
        } else {
          this.authService.authenticateUser(response._body, ()=>{
            this.app.getRootNav().push(AuthComponent).then(() => {
              this.loaderService.handleLoader(false);
              this.app.getRootNav().remove(0, 1);
            });;
          });
          loginForm.resetForm();
          return;
        }
        this.loaderService.handleLoader(false);
      }
    );
  }
}
